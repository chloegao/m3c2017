"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples"""
    from numpy.random import randn

    X = np.zeros((Nt+1,M))
    N = np.sqrt(dt)*randn(Nt+1, M)
    X = np.cumsum(N, axis=0)

    # for j in range(Nt):
  		# X[j+1,:] = X[j,:] + N(j+1, :)

    Xm = np.mean(X,axis=1)
    Xv = np.var(X,axis=1)
    return X,Xm,Xv

def analyze(display=False):
    """Complete this function to analyze simulation error"""
    t = 100
    Mvalues = [10,100,1000,2000]
    error = np.zeros(len(Mvalues))
    for i, M in enumerate(Mvalues):
        X, Xm, Xv=brown1(100, M)
        e = np.absolute(t - Xv[100])
        error[i] = e

    return error, Mvalues

if __name__ == "__main__":
    error, Mvalues = analyze(False)
    print(error)
    plt.plot(Mvalues, error)
    plt.show()

